
Base/
 Doesn't require dependencies, uses pure lua, breaks posix

Base/Simpler/
 More features than Base

Simple/
 requires dependencies, does not adhere to POSIX, Base but with libraries

Robust/
 links to the most featureful tools.

Plan9/
 Mimics plan9 tooling

POSIX/
 Mimics POSIX

